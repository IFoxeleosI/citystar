// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DipSitGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIPSIT_API ADipSitGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
